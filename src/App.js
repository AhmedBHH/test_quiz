import React, { useState, useEffect } from 'react';

const TriviaGame = () => {
  const [difficulty, setDifficulty] = useState('');
  const [questions, setQuestions] = useState([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState('');
  const [showResult, setShowResult] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);
  const [countdown, setCountdown] = useState(10);

  const handleDifficultyChange = (event) => {
    setDifficulty(event.target.value);
  };

useEffect(() => {
  if (difficulty !== '') {
    fetchQuestions();
  }
}, [difficulty]);

  const fetchQuestions = async () => {
    try {
      const response = await fetch(
        `https://opentdb.com/api.php?amount=5&difficulty=${difficulty}&type=multiple`
      );
      const data = await response.json();
      setQuestions(data.results);
    } catch (error) {
      console.error('Error fetching questions:', error);
    }
  };

  const handleAnswer = (answer) => {
    setSelectedAnswer(answer);
  };

  const handleNextQuestion = () => {
    if (selectedAnswer === questions[currentQuestionIndex].correct_answer) {
      setIsCorrect(true);
    } else {
      setIsCorrect(false);
    }
    setShowResult(true);
    setTimeout(() => {
      setShowResult(false);
      setSelectedAnswer('');
      if (currentQuestionIndex < questions.length - 1) {
        setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
      } else {
        // Game over logic can be added here
      }
      setCountdown(10);
    }, 2000);
  };

  useEffect(() => {
    const timer = setInterval(() => {
      setCountdown((prevCountdown) => prevCountdown - 1);
    }, 1000);

    return () => clearInterval(timer);
  }, [currentQuestionIndex]);

  const currentQuestion = questions[currentQuestionIndex];

  return (
    <div>
      {difficulty === '' ? (
        <div>
          <h1>Select Difficulty</h1>
          <select value={difficulty} onChange={handleDifficultyChange}>
            <option value="">-- Select Difficulty --</option>
            <option value="Easy">Easy</option>
            <option value="Medium">Medium</option>
            <option value="Hhard">Hard</option>
          </select>
        </div>
      ) : currentQuestion ? (
        <div>
          <h1>Trivia Game</h1>
          <h2>Question {currentQuestionIndex + 1}</h2>
          <p>{currentQuestion.question}</p>
          {currentQuestion.incorrect_answers.map((answer, index) => (
            <button
              key={index}
              onClick={() => handleAnswer(answer)}
              disabled={showResult}
            >
              {answer}
            </button>
          ))}
          <button onClick={handleNextQuestion} disabled={!selectedAnswer}>
            Next
          </button>
          {showResult && <p>{isCorrect ? ' Right ' : ' Wrong '}</p>}
          <p>Time left: {countdown} seconds</p>
        </div>
      ) : (
        // Step 2: Loading
        <div>Loading...</div>
      )}
    </div>
  );
};

export default TriviaGame;
